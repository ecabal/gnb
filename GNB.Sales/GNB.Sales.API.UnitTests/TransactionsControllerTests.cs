﻿using GNB.Sales.API.Controllers;
using GNB.Sales.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Xunit;

namespace GNB.Sales.API.UnitTests
{
    public class TransactionsControllerTests
    {
        [Fact]
        public async Task GetBySKU_ReturnsOK_WithAListOfProductTransactions()
        {
            // Arrange
            var mockService = new Mock<ITransactionsService>();
            var mockLogger = new Mock<ILogger<TransactionsController>>();

            mockService.Setup(s => s.GetBySKU(It.IsAny<string>()))
                .Returns(Task.FromResult(GetProductTransactionsResult()));

            var controller = new TransactionsController(mockService.Object, mockLogger.Object);

            // Act
            var response = await controller.GetBySKU("T2006");

            // Assert            
            var okResult = Assert.IsType<OkObjectResult>(response);
            var transactionsResult = Assert.IsAssignableFrom<ProductTransactionsResult>(okResult.Value);
            Assert.Equal(2, transactionsResult.Transactions.Count());
        }

        [Fact]
        public async Task GetBySKU_ReturnsNotFound_ForNotExistingSKU()
        {
            // Arrange
            var mockService = new Mock<ITransactionsService>();
            var mockLogger = new Mock<ILogger<TransactionsController>>();

            mockService.Setup(s => s.GetBySKU(It.IsAny<string>()))
                .Returns(Task.FromResult(new ProductTransactionsResult()
                {
                    TotalAmount = 0.00m,
                    Transactions = new List<ProductTransactionDto>()
                }));

            var controller = new TransactionsController(mockService.Object, mockLogger.Object);

            // Act
            var response = await controller.GetBySKU("TTT");

            // Assert            
            Assert.IsType<NotFoundResult>(response);
        }

        [Fact]
        public async Task GetBySKU_ReturnsServerError_ForUnexpectedExceptions()
        {
            // Arrange
            var mockService = new Mock<ITransactionsService>();
            var mockLogger = new Mock<ILogger<TransactionsController>>();

            mockService.Setup(s => s.GetBySKU(It.IsAny<string>())).Throws<InvalidOperationException>();

            var controller = new TransactionsController(mockService.Object, mockLogger.Object);

            // Act
            var response = await controller.GetBySKU("TTT");

            // Assert            
            var codeResult = Assert.IsType<StatusCodeResult>(response);
            Assert.Equal((int)HttpStatusCode.InternalServerError, codeResult.StatusCode);
        }

        private ProductTransactionsResult GetProductTransactionsResult()
        {
            return new ProductTransactionsResult()
            {
                TotalAmount = 14.99m,
                Transactions = new List<ProductTransactionDto>()
                {
                    new ProductTransactionDto { SKU = "T2006", Amount = 10.00m, Currency = "USD" },
                    new ProductTransactionDto { SKU = "T2006", Amount = 7.63m, Currency = "EUR" }
                }
            };
        }
    }
}
