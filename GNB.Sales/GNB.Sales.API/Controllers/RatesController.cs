﻿using GNB.Sales.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace GNB.Sales.API.Controllers
{
    [Route("api/rates")]
    public class RatesController : Controller
    {
        private readonly IRatesService ratesService;
        private readonly ILogger<RatesController> logger;

        public RatesController(IRatesService ratesService, ILogger<RatesController> logger)
        {
            this.ratesService = ratesService ?? throw new ArgumentNullException(nameof(ratesService));
            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                logger.LogInformation("Getting all rates");

                var rates = await ratesService.GetAll();
                return Ok(rates);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Exception trying to get all rates");
                return StatusCode(500);
            }

        }
    }
}
