﻿using GNB.Sales.Application;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.Sales.API.Controllers
{
    [Route("api/transactions")]
    public class TransactionsController : Controller
    {
        private readonly ITransactionsService transactionsService;
        private readonly ILogger<TransactionsController> logger;

        public TransactionsController(ITransactionsService transactionsService, ILogger<TransactionsController> logger)
        {
            this.transactionsService = transactionsService ?? throw new System.ArgumentNullException(nameof(transactionsService));
            this.logger = logger ?? throw new System.ArgumentNullException(nameof(logger));
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            try
            {
                logger.LogInformation("Getting all transactions");

                var transactions = await transactionsService.GetAll();
                return Ok(transactions);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Exception trying to get all transactions");
                return StatusCode(500);
            }
        }

        [HttpGet("{sku}")]
        public async Task<IActionResult> GetBySKU(string sku)
        {
            try
            {
                logger.LogInformation($"Getting transactions for SKU: {sku}");

                var transactions = await transactionsService.GetBySKU(sku);

                if (!transactions.Transactions.Any())
                {
                    return NotFound();
                }

                return Ok(transactions);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, $"Exception trying to get transactions for SKU: {sku}");
                return StatusCode(500);
            }
        }

    }
}
