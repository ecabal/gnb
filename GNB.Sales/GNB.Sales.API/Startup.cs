﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using GNB.Sales.Application;
using AutoMapper;
using GNB.Sales.Core;
using GNB.Sales.Infrastructure;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Caching.Memory;

namespace GNB.Sales.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper();
            services.AddMemoryCache();

            services.AddMvc();

            services.AddTransient<ITransactionsService>(
                x => new MemoryCacheTransactionsService(x.GetService<IMemoryCache>(),
                     new TransactionsService(
                         x.GetService<ITransactionsRepository>(),
                         x.GetService<IRatesRepository>(),
                         x.GetService<ICurrencyConverter>(),
                         x.GetService<IMapper>())));

            services.AddTransient<IRatesService>(
               x => new MemoryCacheRatesService(x.GetService<IMemoryCache>(),
                    new RatesService(
                        x.GetService<IRatesRepository>(),
                        x.GetService<IMapper>())));

            services.AddTransient<ICurrencyConverter, CurrencyConverter>();

            services.TryAddTransient<ITransactionsRepository, WSTransactionsRepository>();
            services.TryAddTransient<IRatesRepository, WSRatesRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
        }
    }
}
