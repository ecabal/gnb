﻿namespace GNB.Sales.Application
{
    public class ProductTransactionDto
    {
        public string SKU { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
    }
}
