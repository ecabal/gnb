﻿using System.Collections.Generic;

namespace GNB.Sales.Application
{
    public class ProductTransactionsResult
    {
        public string TotalCurrency { get; set; }
        public decimal TotalAmount { get; set; }
        public IEnumerable<ProductTransactionDto> Transactions { get; set; } = new List<ProductTransactionDto>();

    }
}
