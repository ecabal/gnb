﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Sales.Application
{
    public interface IRatesService
    {
        Task<IEnumerable<ExchangeRateDto>> GetAll();
    }
}
