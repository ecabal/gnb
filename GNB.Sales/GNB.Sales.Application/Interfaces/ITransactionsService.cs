﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Sales.Application
{
    public interface ITransactionsService
    {
        Task<IEnumerable<ProductTransactionDto>> GetAll();
        Task<ProductTransactionsResult> GetBySKU(string sku);
    }
}
