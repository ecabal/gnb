﻿using AutoMapper;
using GNB.Sales.Core;

namespace GNB.Sales.Application
{
    public class EntityToDtoProfile : Profile
    {
        public EntityToDtoProfile()
        {
            CreateMap<ProductTransaction, ProductTransactionDto>();
            CreateMap<ExchangeRate, ExchangeRateDto>();
        }
    }
}
