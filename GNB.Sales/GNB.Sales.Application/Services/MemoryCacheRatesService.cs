﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.Sales.Application
{
    public class MemoryCacheRatesService : IRatesService
    {
        private readonly IMemoryCache cache;
        private readonly IRatesService ratesService;

        public MemoryCacheRatesService(IMemoryCache cache, IRatesService ratesService)
        {
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
            this.ratesService = ratesService ?? throw new ArgumentNullException(nameof(ratesService));
        }

        public async Task<IEnumerable<ExchangeRateDto>> GetAll()
        {
            var result = await ratesService.GetAll();

            if (result.Any())
            {
                cache.Set("ExchangeRates", result);
            }
            else
            {
                result = cache.Get("ExchangeRates") as IEnumerable<ExchangeRateDto>;
            }

            return result;
        }
    }
}
