﻿using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.Sales.Application
{
    public class MemoryCacheTransactionsService : ITransactionsService
    {
        private readonly IMemoryCache cache;
        private readonly ITransactionsService transactionsService;

        public MemoryCacheTransactionsService(IMemoryCache cache, ITransactionsService transactionsService)
        {
            this.cache = cache ?? throw new ArgumentNullException(nameof(cache));
            this.transactionsService = transactionsService ?? throw new ArgumentNullException(nameof(transactionsService));
        }

        public async Task<IEnumerable<ProductTransactionDto>> GetAll()
        {
            var result = await transactionsService.GetAll();

            if (result.Any())
            {
                cache.Set("ProductTransactions", result);
            }
            else
            {
                result = cache.Get("ProductTransactions") as IEnumerable<ProductTransactionDto>;
            }

            return result;
        }

        public async Task<ProductTransactionsResult> GetBySKU(string sku)
        {
            var result = await transactionsService.GetBySKU(sku);

            if (result.Transactions.Any())
            {
                cache.Set($"ProductTransactions.{sku}", result);
            }
            else
            {
                if (cache.TryGetValue($"ProductTransactions.{sku}", out ProductTransactionsResult cached))
                {
                    return cached;
                }
            }

            return result;
        }
    }
}
