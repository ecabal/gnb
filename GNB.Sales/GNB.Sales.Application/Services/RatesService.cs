﻿using AutoMapper;
using GNB.Sales.Core;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Sales.Application
{
    public class RatesService : IRatesService
    {
        private readonly IRatesRepository ratesRepository;
        private readonly IMapper mapper;

        public RatesService(IRatesRepository ratesRepository, IMapper mapper)
        {
            this.ratesRepository = ratesRepository ?? throw new ArgumentNullException(nameof(ratesRepository));
            this.mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }
        public async Task<IEnumerable<ExchangeRateDto>> GetAll()
        {
            return mapper.Map<IEnumerable<ExchangeRateDto>>(await ratesRepository.GetAll());
        }
    }
}
