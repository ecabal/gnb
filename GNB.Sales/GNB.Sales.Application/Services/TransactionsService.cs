﻿using AutoMapper;
using GNB.Sales.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.Sales.Application
{
    public class TransactionsService : ITransactionsService
    {
        private readonly ITransactionsRepository transactionsRepository;
        private readonly IRatesRepository ratesRepository;
        private readonly ICurrencyConverter currencyConverter;
        private readonly IMapper mapper;

        public TransactionsService(
            ITransactionsRepository transactionsRepository,
            IRatesRepository ratesRepository,
            ICurrencyConverter currencyConverter,
            IMapper mapper)
        {
            this.transactionsRepository = transactionsRepository ?? throw new System.ArgumentNullException(nameof(transactionsRepository));
            this.ratesRepository = ratesRepository ?? throw new System.ArgumentNullException(nameof(ratesRepository));
            this.currencyConverter = currencyConverter ?? throw new System.ArgumentNullException(nameof(currencyConverter));
            this.mapper = mapper ?? throw new System.ArgumentNullException(nameof(mapper));
        }

        public async Task<IEnumerable<ProductTransactionDto>> GetAll()
        {
            var transactions = await transactionsRepository.GetAll();
            return mapper.Map<IEnumerable<ProductTransactionDto>>(transactions);
        }

        public async Task<ProductTransactionsResult> GetBySKU(string sku)
        {
            var transactions = await transactionsRepository.GetBySKU(sku);
            if (!transactions.Any()) return new ProductTransactionsResult();

            var rates = await ratesRepository.GetAll();

            var transactionsInEUR = currencyConverter.Convert(transactions, Currencies.Euro, rates);

            var result = new ProductTransactionsResult()
            {
                TotalCurrency = Currencies.Euro,
                TotalAmount = transactionsInEUR.Sum(t => t.Amount),
                Transactions = mapper.Map<IEnumerable<ProductTransactionDto>>(transactionsInEUR)
            };

            return result;
        }
    }
}
