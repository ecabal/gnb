﻿using System.Collections.Generic;
using Xunit;

namespace GNB.Sales.Core.UnitTests.Services
{
    public class CurrencyConverterTests
    {
        [Fact]
        public void ToEuro_CurrencyNotFound_ReturnsNull()
        {
            // Arrange
            var converter = new CurrencyConverter();
            var transaction = new ProductTransaction { SKU = "T2006", Amount = 7.63m, Currency = "ZZZ" };

            // Act
            var result = converter.Convert(transaction, Currencies.Euro, GetRates());

            // Assert
            Assert.Null(result);
        }

        [Fact]
        public void ToEuro_CurrencyInEUR_ReturnsSameAmount()
        {
            // Arrange
            var converter = new CurrencyConverter();
            var transaction = new ProductTransaction { SKU = "T2006", Amount = 7.63m, Currency = "EUR" };

            // Act
            var result = converter.Convert(transaction, Currencies.Euro, GetRates());

            // Assert
            Assert.Equal(transaction.Amount, result.Amount);
        }

        [Fact]
        public void ToEuro_CurrencyDirectConversion_ReturnsAmountConverted()
        {
            // Arrange
            var converter = new CurrencyConverter();
            var transaction = new ProductTransaction { SKU = "T2006", Amount = 7.63m, Currency = "CAD" };

            // Act
            var result = converter.Convert(transaction, Currencies.Euro, GetRates());

            // Assert            
            Assert.Equal((transaction.Amount * 0.732m), result.Amount);
        }

        [Fact]
        public void ToEuro_CurrencyIndirectConversion_ReturnsAmountConverted()
        {
            // Arrange
            var converter = new CurrencyConverter();
            var transaction = new ProductTransaction { SKU = "T2006", Amount = 7.63m, Currency = "USD" };

            // Act
            var result = converter.Convert(transaction, Currencies.CanadianDollar, GetRates());

            // Assert            
            // USD -> EUR -> CAD
            Assert.Equal((transaction.Amount * 0.736m * 1.366m), result.Amount);
        }

        private IEnumerable<ExchangeRate> GetRates()
        {
            return new List<ExchangeRate>
            {
                new ExchangeRate { From =  "EUR",  To  = "USD", Rate = 1.359m },
                new ExchangeRate { From =  "CAD",  To = "EUR", Rate = 0.732m },
                new ExchangeRate { From =  "USD",  To = "EUR", Rate = 0.736m },
                new ExchangeRate { From =  "EUR",  To = "CAD", Rate = 1.366m }
            };
        }
    }
}
