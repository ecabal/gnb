﻿namespace GNB.Sales.Core
{
    public static class Currencies
    {
        public static string Euro = "EUR";
        public static string CanadianDollar = "CAD";
        public static string USDollar = "USD";

    }
}
