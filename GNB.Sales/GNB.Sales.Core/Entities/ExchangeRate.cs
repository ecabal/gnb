﻿namespace GNB.Sales.Core
{
    public class ExchangeRate
    {
        public string From { get; set; }
        public string To { get; set; }
        public decimal Rate { get; set; }
    }
}
