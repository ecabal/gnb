﻿namespace GNB.Sales.Core
{
    public class ProductTransaction
    {
        public string SKU { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

    }
}
