﻿using System.Collections.Generic;

namespace GNB.Sales.Core
{
    public interface ICurrencyConverter
    {
        ProductTransaction Convert(ProductTransaction transaction, string toCurrency, IEnumerable<ExchangeRate> rates);
        IEnumerable<ProductTransaction> Convert(IEnumerable<ProductTransaction> transactions, string toCurrency, IEnumerable<ExchangeRate> rates);
    }
}