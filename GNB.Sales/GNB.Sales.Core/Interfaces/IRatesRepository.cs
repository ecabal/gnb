﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Sales.Core
{
    public interface IRatesRepository
    {
        Task<IEnumerable<ExchangeRate>> GetAll();
    }
}
