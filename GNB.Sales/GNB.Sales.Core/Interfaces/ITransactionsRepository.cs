﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Sales.Core
{
    public interface ITransactionsRepository
    {
        Task<IEnumerable<ProductTransaction>> GetAll();
        Task<IEnumerable<ProductTransaction>> GetBySKU(string sku);
    }
}
