﻿using System.Collections.Generic;
using System.Linq;

namespace GNB.Sales.Core
{
    public class CurrencyConverter : ICurrencyConverter
    {
        public ProductTransaction Convert(ProductTransaction transaction, string toCurrency, IEnumerable<ExchangeRate> rates)
        {
            if (transaction.Currency.Equals(toCurrency))
            {
                return transaction;
            }

            var rate = GetCurrencyConversionRate(transaction.Currency, toCurrency, rates);

            //TODO: Throw bussines exception ?? what to do when there is no rate?
            if (rate == null) return null;

            return new ProductTransaction
            {
                SKU = transaction.SKU,
                Currency = toCurrency,
                Amount = transaction.Amount * rate.Rate
            };
        }

        public IEnumerable<ProductTransaction> Convert(IEnumerable<ProductTransaction> transactions, string toCurrency, IEnumerable<ExchangeRate> rates)
        {
            return transactions.Select(t => Convert(t, toCurrency, rates));
        }

        private ExchangeRate GetCurrencyConversionRate(string fromCurrency, string toCurrency, IEnumerable<ExchangeRate> rates)
        {
            var rate = rates.Where(r => r.From.Equals(fromCurrency) && r.To.Equals(toCurrency)).FirstOrDefault();

            if (rate != null) return rate;

            var fromRates = rates.Where(r => r.From.Equals(fromCurrency));

            foreach (var f in fromRates)
            {
                rate = rates.Where(r => r.From.Equals(f.To) && r.To.Equals(toCurrency)).FirstOrDefault();
                if (rate == null) continue;

                return new ExchangeRate()
                {
                    From = $"{fromCurrency}-{rate.From}-{toCurrency}",
                    To = toCurrency,
                    Rate = f.Rate * rate.Rate
                };
            }

            return rate;
        }
    }
}
