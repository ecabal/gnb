﻿using GNB.Sales.Core;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace GNB.Sales.Infrastructure
{
    public class InMemoryRatesRepository : IRatesRepository
    {
        public async Task<IEnumerable<ExchangeRate>> GetAll()
        {
            return await Task.FromResult(new List<ExchangeRate>
            {
                new ExchangeRate { From =  "EUR",  To  = "USD", Rate = 1.359m },
                new ExchangeRate { From =  "CAD",  To = "EUR", Rate = 0.732m },
                new ExchangeRate { From =  "USD",  To = "EUR", Rate = 0.736m },
                new ExchangeRate { From =  "EUR",  To = "CAD", Rate = 1.366m }
            });
        }
    }
}
