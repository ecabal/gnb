﻿using GNB.Sales.Core;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace GNB.Sales.Infrastructure
{
    public class InMemoryTransactionsRepository : ITransactionsRepository
    {
        public List<ProductTransaction> Transactions = new List<ProductTransaction>()
            {
                new ProductTransaction { SKU = "T2006", Amount = 10.00m, Currency = "USD" },
                new ProductTransaction { SKU = "M2007", Amount = 134.57m, Currency = "CAD" },
                new ProductTransaction { SKU = "R2008", Amount = 17.95m, Currency = "USD" },
                new ProductTransaction { SKU = "T2006", Amount = 7.63m, Currency = "EUR" },
                new ProductTransaction { SKU = "B2009", Amount = 21.23m, Currency = "USD" }
            };

        public async Task<IEnumerable<ProductTransaction>> GetAll() =>
            await Task.FromResult(Transactions);

        public async Task<IEnumerable<ProductTransaction>> GetBySKU(string sku) =>
            await Task.FromResult(Transactions.Where(t => t.SKU == sku));

    }
}
