﻿using GNB.Sales.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace GNB.Sales.Infrastructure
{
    public class WSRatesRepository : IRatesRepository
    {
        private readonly string ratesUrl;
        private readonly ILogger<WSRatesRepository> logger;

        public WSRatesRepository(IConfiguration configuration, ILogger<WSRatesRepository> logger)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }

            ratesUrl = configuration["ServiceAddress:Rates"];

            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<IEnumerable<ExchangeRate>> GetAll()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetAsync(ratesUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseResult = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<IEnumerable<ExchangeRate>>(responseResult);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception trying to get all rates");
                }

                return new List<ExchangeRate>();
            }
        }
    }
}
