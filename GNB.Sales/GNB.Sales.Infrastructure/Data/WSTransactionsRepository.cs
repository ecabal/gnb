﻿using GNB.Sales.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace GNB.Sales.Infrastructure
{
    public class WSTransactionsRepository : ITransactionsRepository
    {
        private readonly string transactionsUrl;
        private readonly ILogger<WSTransactionsRepository> logger;

        public WSTransactionsRepository(IConfiguration configuration, ILogger<WSTransactionsRepository> logger)
        {
            if (configuration == null)
            {
                throw new ArgumentNullException(nameof(configuration));
            }
            transactionsUrl = configuration["ServiceAddress:Transactions"];

            this.logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }
        public async Task<IEnumerable<ProductTransaction>> GetAll()
        {
            using (var client = new HttpClient())
            {
                try
                {
                    var response = await client.GetAsync(transactionsUrl);

                    if (response.IsSuccessStatusCode)
                    {
                        var responseResult = await response.Content.ReadAsStringAsync();
                        return JsonConvert.DeserializeObject<IEnumerable<ProductTransaction>>(responseResult);
                    }
                }
                catch (Exception ex)
                {
                    logger.LogError(ex, "Exception trying to get all transactions");
                }

                return new List<ProductTransaction>();
            }
        }

        public async Task<IEnumerable<ProductTransaction>> GetBySKU(string sku) =>
            (await GetAll()).Where(t => t.SKU == sku);
    }
}
