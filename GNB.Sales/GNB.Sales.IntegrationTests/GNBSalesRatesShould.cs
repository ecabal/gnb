﻿using GNB.Sales.API;
using GNB.Sales.Core;
using GNB.Sales.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using System.Collections.Generic;
using GNB.Sales.Application;
using System.Linq;

namespace GNB.Sales.IntegrationTests
{
    public class GNBSalesRatesShould
    {
        private readonly TestServer server;
        private readonly HttpClient client;

        public GNBSalesRatesShould()
        {
            // Arrange
            server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .ConfigureServices(services =>
                   {
                       services.AddTransient<IRatesRepository, InMemoryRatesRepository>();
                   })
                );
            client = server.CreateClient();
        }

        [Fact]
        public async Task ReturnAllRates()
        {
            // Act
            var response = await client.GetAsync("api/rates");
            response.EnsureSuccessStatusCode();

            // Assert
            var rates = JsonConvert.DeserializeObject<IEnumerable<ExchangeRateDto>>(response.Content.ReadAsStringAsync().Result);
            Assert.Equal(4, rates.Count());
        }
    }
}
