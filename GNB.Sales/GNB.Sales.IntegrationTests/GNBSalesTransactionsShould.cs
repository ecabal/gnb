﻿using GNB.Sales.API;
using GNB.Sales.Application;
using GNB.Sales.Core;
using GNB.Sales.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Xunit;
using Microsoft.Extensions.DependencyInjection;

namespace GNB.Sales.IntegrationTests
{
    public class GNBSalesTransactionsShould
    {
        private readonly TestServer server;
        private readonly HttpClient client;

        public GNBSalesTransactionsShould()
        {
            // Arrange
            server = new TestServer(new WebHostBuilder()
                .UseStartup<Startup>()
                .ConfigureServices(services =>
                {
                    services.AddTransient<ITransactionsRepository, InMemoryTransactionsRepository>();
                    services.AddTransient<IRatesRepository, InMemoryRatesRepository>();
                }));
            client = server.CreateClient();
        }

        [Fact]
        public async Task ReturnAllTransactions()
        {
            // Act
            var response = await client.GetAsync("api/transactions");
            response.EnsureSuccessStatusCode();

            // Assert
            var transactions = JsonConvert.DeserializeObject<IEnumerable<ProductTransactionDto>>(response.Content.ReadAsStringAsync().Result);
            Assert.Equal(5, transactions.Count());
        }

        [Fact]
        public async Task ReturnTransactionsFilterBySKUWithTotalAmount()
        {
            // Act
            var response = await client.GetAsync("api/transactions/T2006");
            response.EnsureSuccessStatusCode();

            // Assert
            var transactionsResult = JsonConvert.DeserializeObject<ProductTransactionsResult>(response.Content.ReadAsStringAsync().Result);
            Assert.Equal(2, transactionsResult.Transactions.Count());
            Assert.Equal(14.99m, transactionsResult.TotalAmount);

        }
    }
}
